package com.example.kyle_.coffeecuisine;

public interface Beverage {

	public String getDescription();
	public double getCost();
	
	
}
