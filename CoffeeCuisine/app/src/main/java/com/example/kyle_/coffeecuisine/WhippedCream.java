package com.example.kyle_.coffeecuisine;

public class WhippedCream extends BeverageModifier{
	
	public WhippedCream(Beverage newBeverage) {
		super(newBeverage);
	
	System.out.println("\nAdded whipped cream");
	}
	
	public String getDescription() {
		return theBeverage.getDescription() + ", Whipped Cream";
	}

	public double getCost() {
		System.out.println("\nWhipped Cream costs " + 0.3 +" cents.");
		return super.cost() + .30;
}
}
