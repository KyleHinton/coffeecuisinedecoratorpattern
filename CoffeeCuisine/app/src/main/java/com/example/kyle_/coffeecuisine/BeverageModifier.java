package com.example.kyle_.coffeecuisine;

public abstract class BeverageModifier implements Beverage {

	protected Beverage theBeverage;
	
	public BeverageModifier(Beverage newBeverage) {
		theBeverage = newBeverage;
	}
	public String getDescription() {
		return theBeverage.getDescription();
	};
	
	public double cost() {
		return theBeverage.getCost();
	}; 
}
