package com.example.kyle_.coffeecuisine;

public class BeverageMaker {

	public static void main(String[] args) {
		
		Beverage newBeverage = new EspressoShot(new WhippedCream(new HotChocolate()));		
		System.out.println("Ingredients: "  + newBeverage.getDescription());
		System.out.println("Price: "  + newBeverage.getCost());
		
		
		Beverage newCoffee = (new WhippedCream(new EspressoShot(new WhippedCream(new DripCoffee()))));
		System.out.println("Ingredients: "  + newCoffee.getDescription());
		System.out.println("Price: "  + newCoffee.getCost());
		
		Beverage newLatte = (new EspressoShot(new EspressoShot(new WhippedCream(new Latte()))));
		System.out.println("Ingredients: "  + newLatte.getDescription());
		System.out.println("Price: "  + newLatte.getCost());		
		
	}
	
	
}
