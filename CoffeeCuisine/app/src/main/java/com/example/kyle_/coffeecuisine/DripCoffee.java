package com.example.kyle_.coffeecuisine;

public class DripCoffee implements Beverage {

	private String description = "\nDelicious Drip Coffee";
	private double cost = 2.00;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getCost() {
		System.out.println("\nCost of Hot Coffee: " + 2.00);
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	
}
