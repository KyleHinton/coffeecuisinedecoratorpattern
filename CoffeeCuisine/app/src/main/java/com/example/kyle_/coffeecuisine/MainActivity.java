package com.example.kyle_.coffeecuisine;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity {

    private Button CoffeeButton, HotChocolateButton, LatteButton;
    private TextView CoffeeOrderText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CoffeeButton = (Button) findViewById(R.id.btnDripCoffee);

        HotChocolateButton = (Button) findViewById(R.id.btnHotChocolate);

        LatteButton = (Button)findViewById(R.id.btnLatte);
        CoffeeOrderText = (TextView) findViewById(R.id.txtBeverageOrder);
    }

    public void CoffeeButtonClick(View view){
        DripCoffee newCoffee = new DripCoffee();
        CoffeeOrderText.setText(CoffeeOrderText.getText() + newCoffee.getDescription());
    }
    public void HotChocButtonClick(View view){
        HotChocolate newHotChoc = new HotChocolate();
        CoffeeOrderText.setText(CoffeeOrderText.getText() + newHotChoc.getDescription());
    }

    public void LatteButtonClick(View view){
        Latte newLatte = new Latte();
        CoffeeOrderText.setText(CoffeeOrderText.getText() + newLatte.getDescription());
    }
}
