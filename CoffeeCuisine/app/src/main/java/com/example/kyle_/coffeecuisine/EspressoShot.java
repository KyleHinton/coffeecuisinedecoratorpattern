package com.example.kyle_.coffeecuisine;

public class EspressoShot extends BeverageModifier {
	
	public EspressoShot(Beverage newBeverage) {
		super(newBeverage);
	
	System.out.println("\nAdded a shot of espresso");
	}
	
	public String getDescription() {
		return theBeverage.getDescription() + ", Espresso";
	}

	public double getCost() {
		System.out.println("\nEspresso shots cost " + 0.5 +" cents each.");
		return super.cost() + .50;
}


	}
